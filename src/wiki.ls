article = location.search.substring 1 or 'index'
xhr = new XMLHttpRequest

create-toc = !->
  headers = content.querySelectorAll 'h2, h3, h4, h5, h6'
  list = document.createElement \ul
  for header in headers
    anchor = document.createElement \a
    name = header.innerHTML.replace ' ' '-'
    anchor.setAttribute \name name
    anchor.setAttribute \style 'color: rgb(40, 40, 40)'
    content.replaceChild anchor, header
    anchor.appendChild header

    link = document.createElement \a
    link.setAttribute \href \# + name
    link.innerHTML = header.innerHTML

    item = document.createElement \li
    item.appendChild link

    list.appendChild item

  list.setAttribute \style 'background-color: #fafdfa; position: fixed; right: 10%; top: 40px'
  content.insertBefore list, content.firstChild

xhr.open \GET 'articles/' + article + '.md' true
xhr.onreadystatechange = !->
  if xhr.ready-state is 4
    content.innerHTML = 'Article doesn\'t exist.'
    if xhr.status is 200
      content.innerHTML = markdown.toHTML xhr.response-text
      create-toc!

xhr.send!