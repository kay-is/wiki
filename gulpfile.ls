require! [\gulp \gulp-livescript \gulp-uglify \gulp-concat \gulp-minify-css]

cssSource = './lib/style.css'

lsSource = './src/wiki.ls'

jsSource =
  './build/wiki.js'
  './lib/markdown.min.js'

gulp.task \css ->
  gulp.src cssSource
    .pipe gulp-minify-css!
    .pipe gulp.dest './build'

gulp.task \compile ->
  gulp.src lsSource
    .pipe gulp-livescript!
    .pipe gulp.dest './build'

gulp.task \merge [\compile] ->
  gulp.src jsSource
    .pipe gulp-concat 'wiki.js'
    .pipe gulp-uglify!
    .pipe gulp.dest './build'

gulp.task \build [\merge \css]

gulp.task \default [\build]